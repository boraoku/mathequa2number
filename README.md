# mathEqua2Number

A simple Mac and Linux command line program to take command line argument as input (one at a time) and evaluate it as a mathematical expression and return the result.

This Swift code (v4.2) is using [Expressions framework](https://github.com/nicklockwood/Expression).

Example use:
```bash
user@machine:~/folder# ./run "19-(23-(100/5))*2"
Result for 19-(23-(100/5))*2 is:
result is 13.0
```