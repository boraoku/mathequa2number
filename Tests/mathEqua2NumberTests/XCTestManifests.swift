import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(mathEqua2NumberTests.allTests),
    ]
}
#endif