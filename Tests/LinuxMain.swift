import XCTest

import mathEqua2NumberTests

var tests = [XCTestCaseEntry]()
tests += mathEqua2NumberTests.allTests()
XCTMain(tests)