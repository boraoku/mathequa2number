import Foundation
import Expression

func mathEquationStringToDouble(_ equation:String) -> Double? {

    // try check if equation input is already a double
    guard let value = Double(equation) else {

        // if not a double then try to evaluate the expression as an equation with its rest
        do {
            let result = try Expression(equation).evaluate()
            return result
        } catch {
            // not a value or a equation with result, therefore return nil
            return nil    
        }      
    }

    return value
}

func run(_ input:String) {
    guard let value = mathEquationStringToDouble(input) else {
        print("Provided argument is not a valid number or equation.")
        return
    }
    print("result is \(value)")
}

let arguments: [String] = CommandLine.arguments

if arguments.count < 1 {
    print("Please provide math equation as command line argument.")
} else {
    print("Result for \(arguments[1]) is:")
    run(arguments[1])
}
